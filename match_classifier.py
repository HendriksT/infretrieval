import sklearn as sk
import numpy as np
from sklearn import datasets
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import KFold
import pandas as pd

FILES = ["data/modelv2/test_2songs_v2_all.csv", "data/modelv2/test_2songs_v2_none.csv", "data/modelv2/test_clean_v2_all.csv", 
         "data/modelv2/test_clean_v2_none.csv", "data/modelv2/test_noise_v2_all.csv", "data/modelv2/test_noise_v2_none.csv"]

def test_classifiers():
    X, Y, raw = load_data(FILES)

    kf = KFold(n_splits=5)

    classifiers = [
        KNeighborsClassifier(3),
        SVC(kernel="linear", C=0.025),
        SVC(C=0.025),
        RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
        GaussianNB(),
        MLPClassifier(alpha=1),
    ]

    for c in classifiers:
        accs = []
        for train, test in kf.split(X):
            X_train, X_test = X.iloc[train], X.iloc[test]
            Y_train, Y_test = Y.iloc[train], Y.iloc[test]

            y_pred = c.fit(X_train, Y_train).predict(X_test)
            acc = (Y_test == y_pred).sum() / float(len(y_pred))
            # print "acc", acc
            accs.append(acc)
        print "kfold", type(c), np.array(accs).mean()
        # y_pred = c.fit(X, Y).predict(X)
        # acc = (Y == y_pred).sum() / float(len(y_pred))
        # print "combined", acc

def load_data(files):
    df = pd.concat(map(pd.read_csv, files))
    raw = df.copy()
    Y = df["target"]
    del df["Unnamed: 0"], df["in_database"], df["target"]
    return df, Y, raw

def get_confusion_matrix(y_test, y_pred):
    y_test_inv = (y_test + 1) % 2
    y_pred_inv = (y_pred + 1) % 2

    TP = (y_test     & y_pred).sum()
    FP = (y_test_inv & y_pred).sum()
    TN = (y_test_inv & y_pred_inv).sum()
    FN = (y_test     & y_pred_inv).sum()

    return TP, FP, TN, FN

def test_classifiers2():
    X, Y, raw = load_data(FILES)

    classifiers = [
        # KNeighborsClassifier(3),
        SVC(kernel="linear", C=0.025, probability=True),
        # SVC(gamma=2, C=1, probability=True),
        # RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
        # GaussianNB(),
        # MLPClassifier(alpha=1),
    ]


    kf = KFold(n_splits=5)

    for c in classifiers:
        for train, test in kf.split(X):
            X_train, X_test = X.iloc[train], X.iloc[test]
            Y_train, Y_test = Y.iloc[train], Y.iloc[test]

            model = c.fit(X_train, Y_train)
            y_pred_proba = model.predict_proba(X_test)
            y_pred = model.predict(X_test)

            best_p, smallest_FP_FN = .5, float("inf")
            FP_weight, FN_weight = 1, 10

            for p in range(50, 100):
                y_pred_proba_ = (y_pred_proba[:,0] <= float(p) / 100).astype(np.int8)
                TP, FP, TN, FN = get_confusion_matrix(Y_test, y_pred_proba_)

                FP_FN = FP * FP_weight + FN * FN_weight

                if FP_FN < smallest_FP_FN:
                    best_p = p
                    smallest_FP_FN = FP_FN
                    print p, FP, FN

            y_pred_proba = (y_pred_proba[:,0] <= float(best_p) / 100).astype(np.int8)

            for _y_pred in (y_pred, y_pred_proba):
                Y_test_inv = (Y_test + 1) % 2
                y_pred_inv = (_y_pred + 1) % 2

                TP = (Y_test     & _y_pred).sum()
                FP = (Y_test_inv & _y_pred).sum()
                TN = (Y_test_inv & y_pred_inv).sum()
                FN = (Y_test     & y_pred_inv).sum()

                print "========="
                print type(c)
                print "========="
                print "TP", TP
                print "FP", FP
                print "TN", TN
                print "FN", FN

                acc = (TP + TN) / float(len(_y_pred))
                print "ACC", acc

DROP_COUNT = range(2, 10)
DROP_SECOND_COUNT = range(2, 10)
DROP_TOTAL_COUNT = range(2, 10)

def drop_cols(df):
    for i in DROP_COUNT:
        del df["match_count_"+str(i)]
    for i in DROP_SECOND_COUNT:
        del df["match_second_count_"+str(i)]
    for i in DROP_TOTAL_COUNT:
        del df["total_match_count_"+str(i)]

def get_model():
    df = pd.concat(map(pd.read_csv, FILES))

    Y = df["target"]
    del df["Unnamed: 0"], df["in_database"], df["target"]

    drop_cols(df)

    c = SVC(kernel="linear", C=0.025, probability=True)
    return c.fit(df, Y)

def test_overlay():
    model = get_model()

    overlays =  ["data/modelv2/overlay_v2_none_{}.csv".format(str(n)) for n in range(1, 7)]
    overlays += ["data/modelv2/overlay_v2_half_{}.csv".format(str(n)) for n in range(1, 7)]
    overlays += ["data/modelv2/overlay_v2_full_{}.csv".format(str(n)) for n in range(1, 7)]

    accs = []
    TP_total = 0
    FP_FN_total = 0

    for f in overlays:
        test = pd.read_csv(f)
        Y_test = test["target"]
        del test["Unnamed: 0"], test["in_database"], test["target"]

        drop_cols(test)

        y = model.predict_proba(test)
        y = (y[:,0] <= .65).astype(np.int8)
        TP, FP, TN, FN = get_confusion_matrix(Y_test, y)

        print f
        # print "TP", TP
        # print "FP", FP
        # print "TN", TN
        # print "FN", FN
        print "ACC", (TP + TN) / float(len(y))

        accs.append((TP + TN) / float(len(y)))
        TP_total += TP
        FP_FN_total += FP + FN

    print TP_total, FP_FN_total, sum(accs) / len(accs)


if __name__ == "__main__":
    # test_overlay()
    test_classifiers()