from librosa import display
display.specshow(librosa.amplitude_to_db(peaks, ref=np.max), y_axis='linear', x_axis='time')
plt.title('Power spectrogram')
plt.colorbar(format='%+2.0f dB')
plt.tight_layout()
plt.show()