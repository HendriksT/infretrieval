from audfprint import audfprint_analyze
import numpy as np
import os
import sqlite3
import zlib
import sys
from settings import create_analyzer, unpack
from collections import defaultdict
from time import time as tt
from multiprocessing import Pool, freeze_support

def get_files_in(dir):
    for a, _, d in os.walk(dir):
        for f in d:
            yield os.path.join(a, f)

def mp3_file_names(dir):
    for f in get_files_in(dir):
        if f.endswith(".mp3") or f.endswith(".m4a"):
            yield f

def npz_file_names(dir):
    for f in get_files_in(dir):
        if f.endswith(".npz"):
            yield f

NPZ = list(npz_file_names(sys.argv[2]))

def create_and_save_hash((f, in_dir, out_dir)):
    name = os.path.split(f)[-1] + ".npz"
    out = os.path.join(out_dir, name)
    if out not in NPZ:
        print f
        analyzer = create_analyzer(shifts=4)
        time_hashes = analyzer.wavfile2hashes(f)

        np.savez_compressed(out, data=time_hashes)
    else:
        print "skipping", f

if __name__ == '__main__':
    freeze_support()

    in_dir = sys.argv[1]
    out_dir = sys.argv[2]

    files = list((f, in_dir, out_dir) for f in mp3_file_names(in_dir))

    p = Pool(4)
    p.map(create_and_save_hash, files)