from audfprint import audfprint_analyze

BUCKET_SIZE_WIDTH = 7 # bucket size of 128
DOC_ID_MAX_WIDTH = 14 # max of 16k docids
DOC_ID_COMPACT_WIDTH = DOC_ID_MAX_WIDTH - BUCKET_SIZE_WIDTH
BUCKET_SIZE = 2**BUCKET_SIZE_WIDTH
HASH_WIDTH = 20

"""
  -d <dbase>, --dbase <dbase>     Fingerprint database file
  -n <dens>, --density <dens>     Target hashes per second [default: 20.0]
  -h <bits>, --hashbits <bits>    How many bits in each hash [default: 20]
  -b <val>, --bucketsize <val>    Number of entries per bucket [default: 100]
  -t <val>, --maxtime <val>       Largest time value stored [default: 16384]
  -u <val>, --maxtimebits <val>   maxtime as a number of bits (16384 == 14 bits)
  -r <val>, --samplerate <val>    Resample input files to this [default: 11025]
  -p <dir>, --precompdir <dir>    Save precomputed files under this dir [default: .]
  -i <val>, --shifts <val>        Use this many subframe shifts building fp [default: 0]
  -w <val>, --match-win <val>     Maximum tolerable frame skew to count as a match [default: 2]
  -N <val>, --min-count <val>     Minimum number of matching landmarks to count as a match [default: 5]
  -x <val>, --max-matches <val>   Maximum number of matches to report for each query [default: 1]
  -X, --exact-count               Flag to use more precise (but slower) match counting
  -R, --find-time-range           Report the time support of each match
  -Q, --time-quantile <val>       Quantile at extremes of time support [default: 0.05]
  -S <val>, --freq-sd <val>       Frequency peak spreading SD in bins [default: 30.0]
  -F <val>, --fanout <val>        Max number of hash pairs per peak [default: 3]
  -P <val>, --pks-per-frame <val>  Maximum number of peaks per frame [default: 5]
  -D <val>, --search-depth <val>  How far down to search raw matching track list [default: 100]
  -H <val>, --ncores <val>        Number of processes to use [default: 1]
  -o <name>, --opfile <name>      Write output (matches) to this file, not stdout [default: ]
  -K, --precompute-peaks          Precompute just landmarks (else full hashes)
  -k, --skip-existing             On precompute, skip items if output file already exists
  -C, --continue-on-error         Keep processing despite errors reading input
  -l, --list                      Input files are lists, not audio
  -T, --sortbytime                Sort multiple hits per file by time (instead of score)
  -v <val>, --verbose <val>       Verbosity level [default: 1]
  -I, --illustrate                Make a plot showing the match
  -J, --illustrate-hpf            Plot the match, using onset enhancement
  -W <dir>, --wavdir <dir>        Find sound files under this dir [default: ]
  -V <ext>, --wavext <ext>        Extension to add to wav file names [default: ]
  --version                       Report version number
  --help                          Print this message
"""

def create_analyzer(shifts=1):
    # Create analyzer object; parameters will get set below
    analyzer = audfprint_analyze.Analyzer()
    # Read parameters from command line/docopts
    analyzer.density = 20.0
    analyzer.maxpksperframe = 5
    analyzer.maxpairsperpeak = 3
    analyzer.f_sd = 30.0
    analyzer.shifts = shifts # 4 for matching 1 for construction db
    # fixed - 512 pt FFT with 256 pt hop at 11025 Hz
    analyzer.target_sr = 11025
    analyzer.n_fft = 512
    analyzer.n_hop = analyzer.n_fft/2
    analyzer.fail_on_error = False
    return analyzer

def unpack(packed, position):
    docid = ((packed % 2**DOC_ID_COMPACT_WIDTH) << BUCKET_SIZE_WIDTH) | position
    time = packed >> DOC_ID_COMPACT_WIDTH
    return time, docid