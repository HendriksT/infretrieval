#White noise mp3 taken from http://mc2method.org/white-noise/
#Conversations taken from
# TODO

import multiprocessing
import sys
import os
import random
import argparse
import pyaudio,wave
import threading
from pydub import * #http://pydub.com/
from random import randint
from time import sleep
from subprocess import Popen, PIPE

PATH_TO_MUSIC       = "/home/uauser/Desktop/RetrievalMusic/"
PATH_TO_OUT         = "/home/uauser/Desktop/TestClips/"
NUMBER_OF_TESTS     = 1
TEST_CLIP_LENGTHS   = [4000,7000,10000]
OUTPUT_DEVICE_INDEX = 1

RECORDING_DEVICE = "Microfoon (Blue Snowball)"
VLC_PATH = "C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe"
VLC_PROCESS_NAME = "vlc.exe"

COMMAND_RECORD    = """"{vlc_path}" -I dummy dshow:// :dshow-vdev="None" :dshow-adev="{mic_name}" :sout=#transcode{{acodec=flac}}:std{{access=file,dst="{recording_path}"}} --run-time {duration} vlc://quit"""
COMMAND_PLAY      = """"{vlc_path}" -I dummy "{song_path}" --start-time {start_offset} --stop-time {end_offset} vlc://quit"""
COMMAND_PROCESSES = """TASKLIST"""

def get_running_processes():
    p = Popen(COMMAND_PROCESSES, stdout=PIPE)
    result = p.stdout.read().split("\n")[2:]
    p.kill()
    
    lengths = [len(d) + 1 for d in result[0].split(" ")]
    out = []
    for r in result[1:]:
        row = []
        for offset in range(len(lengths)):
            start = sum(lengths[:offset])
            end   = sum(lengths[:offset+1])
            row.append(r[start:end-1].strip())
        if any(map(len, row)):
            out.append(row)
    return out

def get_process_count(process_name):
    running_processes = get_running_processes()
    return sum(p[0] == process_name for p in running_processes)

def play_and_record(song_path, out_path, start_offset, duration, mic_name):
    cmd_play = COMMAND_PLAY.format(vlc_path=VLC_PATH, song_path=song_path, start_offset=start_offset, end_offset=start_offset+duration)
    cmd_reco = COMMAND_RECORD.format(vlc_path=VLC_PATH, mic_name=RECORDING_DEVICE, recording_path=out_path, duration=duration)

    print cmd_play
    print cmd_reco

    play_process = Popen(cmd_play)
    reco_process = Popen(cmd_reco)
    sleep(.1)

    process_count = get_process_count(VLC_PROCESS_NAME)
    if process_count != 2:
        print "killing processes"
        play_process.kill()
        reco_process.kill()

        try:
            os.remove(out_path)
        except:
            pass
    else:
        play_process.wait()
        reco_process.wait()

    sleep(.1)

def play_and_record_random_sample(song_path, out_path, duration, mic_name):
    total_seconds = 3 * 60.0
    offset = randint(0, total_seconds - duration)
    play_and_record(song_path, out_path, offset, duration, mic_name)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--musicfolder', dest="PATH_TO_MUSIC", required=False,default = PATH_TO_MUSIC)
    parser.add_argument('-o', '--outputfolder', dest="PATH_TO_OUT", required=False,default = PATH_TO_OUT)
    parser.add_argument('-nr', '--numbertests', type=int,dest="NUMBER_OF_TESTS", required=False,default = NUMBER_OF_TESTS)
    parser.add_argument('-d', '--device', type=int,dest="OUTPUT_DEVICE_INDEX", required=False,default = OUTPUT_DEVICE_INDEX)
    args = parser.parse_args()

    #Reduce length of noise files from 10m to 10s
    #reduce_length_of_noises()

    args.PATH_TO_OUT   += "/" if args.PATH_TO_OUT[-1]   != '/' else ""
    args.PATH_TO_MUSIC += "/" if args.PATH_TO_MUSIC[-1] != '/' else ""

    PATH_TO_OUT   = args.PATH_TO_OUT  
    PATH_TO_MUSIC = args.PATH_TO_MUSIC
    OUTPUT_DEVICE_INDEX = args.OUTPUT_DEVICE_INDEX

    for _ in range(args.NUMBER_OF_TESTS):
        test_clip_length = TEST_CLIP_LENGTHS[randint(0,2)]

        # Select a random file from the listed music directory
        original_mp3_path = PATH_TO_MUSIC + random.choice(os.listdir(PATH_TO_MUSIC))

        while not original_mp3_path.endswith(".mp3") and not original_mp3_path.endswith(".m4a"):
            original_mp3_path = PATH_TO_MUSIC + random.choice(os.listdir(PATH_TO_MUSIC))

        output_filename = args.PATH_TO_OUT+original_mp3_path.split('/')[-1]
        output_filename = ".".join(output_filename.split('.')[:-1])+'_test'+str(_)+"_"+str(test_clip_length)+'_rerecorded.flac'
        open(output_filename, "w+").close()

        play_and_record_random_sample(
            song_path = original_mp3_path, 
            out_path = output_filename, 
            duration = test_clip_length / 1000, 
            mic_name = RECORDING_DEVICE
        )