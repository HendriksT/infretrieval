import sys
import time
import os
import argparse


# Access to match functions, used in command line interface
import audfprint_match
import audfprint_analyze
import hash_table

PATH_TO_SAMPLES = "/home/uauser/Desktop/TestClips/smallsample/" #or rerecorded
PATH_TO_HASHES = "/home/uauser/Desktop/all_hashes_subset/"

class Result():
    def __init__(self,resType, testName, in_db_state):
        self.resType = resType
        self.testName = testName.split('/')[-1]
        self.in_db = in_db_state

    def __str__(self):
        return self.resType + " " + self.testName  + " --- was it in db?   " + str(self.in_db)

def file_names(dir, extensions=["m4a","wav","mp3"]):
    for a, _, d in os.walk(dir):
        for f in d:
            if any(f.endswith(ext) for ext in extensions):
                yield os.path.join(a, f)

def create_hashtable(hashes_location,analyzer):
    #also using default params from audfprint
    hash_tab = hash_table.HashTable(
        hashbits=20,#int(args['--hashbits']),
        depth=100,#int(args['--bucketsize']),
        maxtime=16384)#(1 << int(args['--maxtimebits']))) actually its 14 then 1 << 14

    hashfiles = file_names(hashes_location, [".m4a.npz"])

    # Adding files
    tothashes = 0
    ix = 0

    for filename in hashfiles :
        print([time.ctime() + " ingesting #" + str(ix) + ": "
                + filename + " ..."])
        dur, nhash = analyzer.ingest(hash_tab, filename)
        tothashes += nhash
        ix += 1

    print(analyzer.soundfiletotaldur)
    print(["Added " +  str(tothashes) + " hashes "
            + "(%.1f" % (tothashes/float(analyzer.soundfiletotaldur))
            + " hashes/sec)"])
    return hash_tab


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--samplesfolder', dest="PATH_TO_SAMPLES", required=False,default = PATH_TO_SAMPLES)
    parser.add_argument('-db', '--dbfolder', dest="PATH_TO_HASHES", required=False,default = PATH_TO_HASHES)
    parser.add_argument('-v', '--verbose', dest="VERBOSE", required=False, default=False)
    args = parser.parse_args()

    #With default parameters from the audf script.
    #TODO make them configurable. when we test with a threshold of 4 (or whatever) this tool maybe should be set that way too

    matcher = audfprint_match.Matcher()
    matcher.window = 2
    matcher.threshcount = 5
    matcher.max_returns = 1
    matcher.search_depth = 100
    matcher.sort_by_time = False
    matcher.exact_count = False
    matcher.illustrate = False
    matcher.illustrate_hpf = False
    matcher.verbose = 1
    matcher.find_time_range = False
    matcher.time_quantile = 0.05

    analyzer = audfprint_analyze.Analyzer()
    analyzer.density = 20.0
    analyzer.maxpksperframe = 5
    analyzer.maxpairsperpeak = 3
    analyzer.f_sd = 30.0
    analyzer.shifts = 0
    analyzer.target_sr = 11025
    analyzer.n_fft = 512
    analyzer.n_hop = analyzer.n_fft/2
    if analyzer.shifts == 0:
        analyzer.shifts = 4 #4 for matching, otherwise 1
    analyzer.fail_on_error = True

    hash_tab = create_hashtable(args.PATH_TO_HASHES,analyzer)

    num = 0
    list_of_result_tuples = [] #tuples match, testname, possibly if its in db or not (TODO)
    for f in file_names(args.PATH_TO_SAMPLES):
        msgs = matcher.file_match_to_msgs(analyzer, hash_tab, f, num)
        if (msgs[0].split(' ')[0] == 'Matched'):
            list_of_result_tuples.append(Result("MATCH  ", f, False))
        else:
            list_of_result_tuples.append(Result("NOMATCH", f, False))


        if (args.VERBOSE):
            for msg in msgs:
                print(msg)
    for l in list_of_result_tuples:
        print(l)

        num += 1
