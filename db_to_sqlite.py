import sqlite3
import numpy as np
import zlib

conn = sqlite3.connect('data/songs400.db')
conn.execute("DROP TABLE fingers")
conn.commit()
conn.execute("CREATE TABLE fingers (hsh INT PRIMARY KEY, packs BLOB)")
conn.commit()

db = np.load("data/db.npy")

c = 0

insert_data = []

for hsh, row in enumerate(db):
    if np.count_nonzero(row):
        c += 1
        insert_data.append((hsh, sqlite3.Binary(zlib.compress(row.tobytes()))))

conn.executemany("INSERT INTO fingers (hsh, packs) VALUES (?,?)", insert_data)
conn.commit()

print "rows", len(db)
print "nonempty", c
print c / float(len(db))