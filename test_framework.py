from audfprint import audfprint_analyze, audfprint_match, hash_table
import numpy as np
import os
from collections import defaultdict, Counter
from itertools import izip
from match_classifier import get_model, drop_cols
import pandas as pd
import time
import argparse

from settings import create_analyzer, unpack, \
    BUCKET_SIZE_WIDTH, DOC_ID_MAX_WIDTH, DOC_ID_COMPACT_WIDTH, BUCKET_SIZE, HASH_WIDTH

FILTER_MUSIC = [".wav", ".mp3", ".m4a", ".flac"]
FILTER_HASH  = [".npz"]

def parse_args():
    parser_types = {
        "ears": EarsBenchmarker,
        "audf": AudfprintBenchmarker
    }

    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--type', dest="parser_type", required=True, choices=parser_types.keys())

    parser.add_argument('-db', '--dbtrain', dest="db_train", required=False, default=None)
    parser.add_argument('-ma', '--musictraindir', dest="musicdir_train", required=False, default=None)
    parser.add_argument('-ha', '--hashtraindir', dest="hashdir_train", required=False, default=None)
    parser.add_argument('-me', '--musictestdir', dest="musicdir_test", required=False, default=None)
    parser.add_argument('-he', '--hashtestdir', dest="hashdir_test", required=False, default=None)
    parser.add_argument('-o', '--outdir', dest="outdir", required=False, default=None)

    parser.add_argument('-ex', '--excludedir', dest="excludedir", required=False, default=None)
    parser.add_argument('-frac', '--fractrain', dest="fractrain", required=False, default=1.0)
    parser.add_argument('-mode', '--trainmode', dest="trainmode", required=False, default="create") # other: append
    parser.add_argument('-csv', '--csvmatches', dest="csvmatches", required=False, default=None) # other: append

    args = parser.parse_args()

    args.parser_type = parser_types[args.parser_type]
    return args

class Benchmarker(object):
    """docstring for Benchmarker"""
    def __init__(self, machinelearning):
        super(Benchmarker, self).__init__()
        self.machinelearning = machinelearning

    def run(self):
        args = parse_args()

        if args.db_train == None and args.musicdir_train == None and args.hashdir_train == None:
            print "Need to specify either --dbtrain (-db) or --musictraindir (-ma) or --hashtraindir (-ha)"
            exit(1)
        if args.musicdir_test == None and args.hashdir_test == None:
            print "Need to specify either --musictestdir (-me) or --hashtestdir (-he)"
            exit(1)

        print "Creating database..."
        self.create(args)

        print "Checking test data..."
        self.test(args)

    def create(self, args):
        if args.db_train != None and args.trainmode == "create":
            self.load_db(args.db_train)
        else:
            exclude = []
            if args.excludedir != None:
                exclude = list(self.file_names(args.excludedir, FILTER_MUSIC + FILTER_HASH))

            train_dir = args.musicdir_train
            reader_train = self.music_reader()
            filter_train = FILTER_MUSIC
            if args.hashdir_train != None:
                reader_train = self.hash_reader()
                filter_train = FILTER_HASH
                train_dir = args.hashdir_train

            train_files = list(self.file_names(train_dir, filter_train, exclude))
            train_files = train_files[:int(len(train_files) * float(args.fractrain))]

            if args.trainmode == "append":
                if args.db_train == None:
                    print "Provide a database path with -db (--dbtrain) when using trainmode append"
                    exit(1)
                self.load_db(args.db_train)

            self.create_database_raw(train_files, reader_train)

            # in some cases we might not want to store the DB but just work in-memory
            if args.outdir:
                self.save_db(args.outdir)

    def test(self, args):
        test_dir = args.musicdir_test
        reader_test = self.music_reader(shifts=4)
        filter_test = FILTER_MUSIC
        if args.hashdir_test != None:
            reader_test = self.hash_reader()
            filter_test = FILTER_HASH
            test_dir = args.hashdir_test


        stats = {
            "4000": defaultdict(int),
            "7000": defaultdict(int),
            "10000": defaultdict(int),
            "13000": defaultdict(int),
            "17000": defaultdict(int),
            "23600": defaultdict(int),
        }

        model = get_model()

        dups = []
        test_files = list(self.file_names(test_dir, filter_test))

        feature_list = ["target", "in_database"]
        for i in range(10):
            feature_list += [
                "match_count_" + str(i), 
                "match_second_count_" + str(i), 
                "total_match_count_" + str(i)
            ]

        matches_df = pd.DataFrame(0, index=np.arange(len(test_files)), columns=feature_list)

        for i, (f, time_hashes) in enumerate(reader_test(test_files)):
            matches, total_matches, matches_second = self.match(time_hashes)
            matchee_full = os.path.split(f)[-1]
            matchee_parts = matchee_full.split("_test")
            matchee_name = matchee_parts[0]
            in_database = self.is_in_database(matchee_name)

            if matches:
                docid = matches[0][0]
                filepath = matches[0][2]

                target = 0
                if in_database and matchee_name in filepath:
                    target = 1

                matches_df.set_value(i, "target", target)
                matches_df.set_value(i, "in_database", int(in_database))
                for j, m in enumerate(matches):
                    matches_df.set_value(i, "match_count_" + str(j), m[1])
                for j, m in enumerate(matches_second):
                    matches_df.set_value(i, "match_second_count_" + str(j), m[1])
                for j, m in enumerate(total_matches):
                    matches_df.set_value(i, "total_match_count_" + str(j), m[1])

                if self.machinelearning:
                    row = matches_df.iloc[[i]]
                    drop_cols(row)
                    del row["target"], row["in_database"]

                    y = model.predict_proba(row)
                    y = (y[:,0] <= .65).astype(np.int8)
                    y = y[0]
                else:
                    y = 1
            else:
                y = 0

            key = "10000"
            for k in stats:
                if "_" + k + "_" in matchee_full:
                    key = k
                    break

            if y == 1:
                if matchee_name in filepath:
                    print "[CORRECT]  ", matchee_name, "->", filepath
                    stats[key]["true_positives"] += 1
                else:
                    print "[INCORRECT]", matchee_name, "->", filepath
                    stats[key]["false_positives"] += 1
            else:
                if in_database:
                    print "[INCORRECT NO MATCH]"
                    stats[key]["false_negatives"] += 1
                else:
                    print "[CORRECT NO MATCH]"
                    stats[key]["true_negatives"] += 1

        if args.csvmatches:
            matches_df.to_csv(args.csvmatches)

        combined = defaultdict(int)

        for d in stats.values():
            for k, v in d.iteritems():
                combined[k] += v

        stats["COMBINED"] = combined
        for k, d in stats.iteritems():
            true_positives, false_positives, true_negatives, false_negatives = d["true_positives"], d["false_positives"], d["true_negatives"], d["false_negatives"]
            total = float(true_positives + false_positives + true_negatives + false_negatives)

            if total == 0: continue

            print "===================="
            print k
            print "===================="
            print "CORRECT", true_positives + true_negatives
            print "INCORRECT", false_positives + false_negatives
            print "NO MATCH", false_negatives + true_negatives
            print "===================="
            print "ACCURACY", (true_positives + true_negatives) / total
            print "===================="
            print "TP", true_positives
            print "FP", false_positives
            print "TN", true_negatives
            print "FN", false_negatives
            print
            print

    def is_in_database(self, name):
        raise NotImplementedError()
        
    def create_database(self, hashes):
        raise NotImplementedError()

    def create_database_raw(self, files, file_reader):
        return self.create_database(file_reader(files))

    def music_reader(self, **kwargs):
        def inner(files):
            analyzer = create_analyzer(**kwargs)

            for f in files:
                yield f, analyzer.wavfile2hashes(f)
        return inner

    def hash_reader(self):
        def inner(files):
            for f in files:
                yield f, np.load(f)["data"]
        return inner

    def file_names(self, dir, extensions, exclude = []):
        for a, _, d in os.walk(dir):
            for f in d:
                if any(f.endswith(ext) for ext in extensions):
                    f_stripped = f
                    for e in extensions:
                        f_stripped = f_stripped.strip(e)
                    if not any(f_stripped in e for e in exclude):
                        yield os.path.join(a, f)

    def match(self, time_hashes):
        raise NotImplementedError()

    def load_db(self, path):
        raise NotImplementedError()

    def save_db(self, path):
        raise NotImplementedError()


class EarsBenchmarker(Benchmarker):
    """docstring for EarsBenchmarker"""
    def __init__(self):
        super(EarsBenchmarker, self).__init__(True)
        self.db = None
        self.hash_counts = None
        self.id_mapping = None
    
    def create(self, args):
        super(EarsBenchmarker, self).create(args)

    def test(self, args):
        self.id_mapping = dict(izip(map(int, self.id_mapping[:,0]), self.id_mapping[:,1]))
        super(EarsBenchmarker, self).test(args)

    def is_in_database(self, name):
        return any(name in filepath for filepath in self.id_mapping.values())

    def create_database(self, hashes):
        if self.db is None:
            # create a database to store hashes
            self.db = np.zeros(shape=(2**HASH_WIDTH, BUCKET_SIZE), dtype=np.uint16)

        if self.hash_counts is None:
            self.hash_counts = np.zeros(2**DOC_ID_MAX_WIDTH, dtype=np.uint32)

        if self.id_mapping is None:
            self.id_mapping = dict()
        else:
            self.id_mapping = dict(izip(map(int, self.id_mapping[:,0]), self.id_mapping[:,1]))

        docid_offset = 0
        if len(self.id_mapping) > 0:
            docid_offset = max(self.id_mapping.keys())

        total_hashes, dropped_hashes, dropped_hashes_self = 0, 0, 0

        for docid, (f, time_hashes) in enumerate(hashes):
            docid += docid_offset
            self.id_mapping[docid] = f

            arr_pos = docid % BUCKET_SIZE
            docid_compact = docid >> BUCKET_SIZE_WIDTH

            if docid % 100 == 0:
                print "indexing", docid
                print "arr_pos", arr_pos
                print "docid", docid
                print "docid_compact", docid_compact

            try:
                total_hashes += time_hashes.shape[0]
            except:
                continue

            hash_packed = []

            for time, hsh in time_hashes:
                time_docid_packed = np.uint16((time << DOC_ID_COMPACT_WIDTH) | docid_compact)
                hash_packed.append((hsh, time_docid_packed))

            hash_packed = set(hash_packed)
            for hsh, time_docid_packed in hash_packed:
                # print "{:b}".format(time), "{:b}".format(docid_compact), "both"
                # print "{:b}".format(time_docid_packed), "packed"

                curr_val = self.db[hsh, arr_pos]
                if curr_val == 0 or curr_val == time_docid_packed: # no collision
                    self.db[hsh, arr_pos] = time_docid_packed
                    self.hash_counts[docid] += 1
                else: # collision
                    # print curr_val, time_docid_packed
                    dropped_hashes += 1

                    _, colliding_doc = unpack(curr_val, arr_pos)

                    if colliding_doc == docid:
                        dropped_hashes_self += 1

                    colliding_hash_count = self.hash_counts[colliding_doc]
                    hash_count = self.hash_counts[docid]

                    rand = np.random.randint(0, hash_count + colliding_hash_count)
                    if rand > hash_count: # replace the existing pair with the new one
                        self.db[hsh, arr_pos] = time_docid_packed
                        self.hash_counts[colliding_doc] -= 1
                        self.hash_counts[docid] += 1
                # print "{:b}".format(self.db[hsh, arr_pos]), "stored"

            if docid % 100 == 0:
                print self.db[hsh]
                print "dropped/total", dropped_hashes, total_hashes, \
                    (float(dropped_hashes)/float(total_hashes)), "self", \
                    float(dropped_hashes_self) / float(total_hashes)

        self.id_mapping = np.array(list(self.id_mapping.iteritems()))

        nonzero = np.count_nonzero(self.db)
        size = self.db.shape[0] * self.db.shape[1]

        print "STATS"

        print "nonzero", nonzero
        print "total", size
        print "%% filled", float(nonzero) / float(size)


    # docid = ((packed % 2**DOC_ID_COMPACT_WIDTH) << BUCKET_SIZE_WIDTH) | position
    # time = packed >> DOC_ID_COMPACT_WIDTH
    # return time, docid

    def match(self, time_hashes):
        time_values = time_hashes[:,0]
        row_indices = time_hashes[:,1]
        packed_rows = self.db[time_hashes[:,1]]
        keep_vals  = np.where(packed_rows != 0)
        positions = np.tile(np.arange(128), (len(row_indices), 1))

        time_values = np.tile(time_values, (128, 1)).T

        row_doc_ids = ((packed_rows % 2**DOC_ID_COMPACT_WIDTH) << BUCKET_SIZE_WIDTH) | positions
        row_time_offsets = (packed_rows >> DOC_ID_COMPACT_WIDTH) - time_values


        # for t, hsh in time_hashes:
        #     for i, packed in enumerate(self.db[hsh]):
        #         if packed != 0:
        #             time_match, docid = unpack(packed, i)
        #             offset = t - time_match
        #             matches[(docid, offset)] += 1
        #             total_matches[docid] += 1

        matches = Counter(izip(row_doc_ids[keep_vals], row_time_offsets[keep_vals]))
        total_matches = Counter(row_doc_ids[keep_vals])

        best_matches = defaultdict(list)
        for (docid, offset), count in matches.iteritems():
            best_matches[docid].append(count)

        for docid in best_matches:
            if len(best_matches[docid]) == 1:
                best_matches[docid].append(0)
            best_matches[docid] = sorted(best_matches[docid])[::-1]

        best_total_matches = sorted(total_matches.iteritems(), key=lambda r: r[1])[::-1]
        best_matches = sorted(best_matches.iteritems(), key=lambda r: r[1][0])[::-1]

        best_matches = best_matches[:10] if len(best_matches) > 10 else best_matches
        best_total_matches = best_total_matches[:10] if len(best_total_matches) > 10 else best_total_matches

        return [(m[0], m[1][0], self.id_mapping[m[0]]) for m in best_matches], \
               [(m[0], m[1], self.id_mapping[m[0]]) for m in best_total_matches], \
               [(m[0], m[1][1], self.id_mapping[m[0]]) for m in best_matches]

    def load_db(self, path):
        self.db = np.load(os.path.join(path, "db.npy"))
        self.hash_counts = np.load(os.path.join(path, "counts.npy"))
        self.id_mapping = np.load(os.path.join(path, "id_mapping.npy"))

    def save_db(self, path):
        np.save(os.path.join(path, "db.npy"), self.db)
        np.save(os.path.join(path, "counts.npy"), self.hash_counts)
        np.save(os.path.join(path, "id_mapping.npy"), self.id_mapping)


class AudfprintBenchmarker(Benchmarker):
    """docstring for EarsBenchmarker"""
    def __init__(self):
        super(AudfprintBenchmarker, self).__init__(False)
        self.db = None
    
    def create(self, args):
        super(AudfprintBenchmarker, self).create(args)

    def test(self, args):
        super(AudfprintBenchmarker, self).test(args)

    def is_in_database(self, name):
        return any(name in filepath for filepath in self.db.names)

    def create_database(self, hashes):
        if self.db is None:
            self.db = hash_table.HashTable(
                hashbits=20,#int(args['--hashbits']),
                depth=100,#int(args['--bucketsize']),
                maxtime=16384)#(1 << int(args['--maxtimebits']))) actually its 14 then 1 << 14

        tothashes = 0

        for i, (filename, time_hashes) in enumerate(hashes):
            if i % 100 == 0:
                print time.ctime() + " ingesting #" + str(i) + ": " + filename + " ..."
            self.db.store(filename, time_hashes)
            tothashes += len(time_hashes)

        print "Added " +  str(tothashes) + " hashes "

    def match(self, time_hashes):
        matcher = audfprint_match.Matcher()
        matcher.window = 2
        matcher.threshcount = 5
        matcher.max_returns = 1
        matcher.search_depth = 100
        matcher.sort_by_time = False
        matcher.exact_count = False
        matcher.illustrate = False
        matcher.illustrate_hpf = False
        matcher.verbose = 1
        matcher.find_time_range = False
        matcher.time_quantile = 0.05

        num = 0
        list_of_result_tuples = [] #tuples match, testname, possibly if its in db or not (TODO)

        matches = matcher.match_hashes(self.db, time_hashes)
        matches_out = []
        matches_total_out = []

        if len(matches) > 0:
            for i, (tophitid, nhashaligned, aligntime, nhashraw, rank, min_time, max_time) in enumerate(matches):
                if i >= 10:
                    break
                matches_out.append((tophitid, nhashaligned, self.db.names[tophitid]))
                matches_total_out.append((tophitid, nhashraw, self.db.names[tophitid]))

        return matches_out, matches_total_out, matches_out

    def load_db(self, path):
        self.db = hash_table.HashTable(filename=os.path.join(path, "db_audfprint.npy"))

    def save_db(self, path):
        self.db.save(os.path.join(path, "db_audfprint.npy"))

def cartesian(*arrays):
    return np.array(np.meshgrid(*arrays)).T.reshape(-1,len(arrays))

def create_kgrams(hashes, distance = 1, k = 2, bits = 20):
    import sha

    def calc_hash(row):
        return np.uint32(int(sha.new(repr(row)).hexdigest()[:bits/4], 16))

    kgrams = []
    total = 0
    found = 0
    for time in set(hashes[:, 0]):
        hash_lists = []
        for i in range(k):
            hash_lists.append(hashes[hashes[:, 0] == time + i * distance][:, 1])
        # print time
        grams = cartesian(*hash_lists)
        if len(grams) > 0:
            found += 1
            grams = np.apply_along_axis(calc_hash, 1, grams)
            kgrams.append(grams)

        total += 1

        # print hash_lists
        # print grams
    # print "total vs has kgrams", total, found
    return np.concatenate(kgrams, axis=0)

KGRAM_BUCKET_SIZE_WIDTH = BUCKET_SIZE_WIDTH
KGRAM_BUCKET_SIZE = 2**KGRAM_BUCKET_SIZE_WIDTH
KGRAM_WIDTH = 3

class KGramBenchmarker(Benchmarker):
    """docstring for KGramBenchmarker"""
    def __init__(self):
        super(KGramBenchmarker, self).__init__(False)
        self.db = None
        self.hash_counts = None
        self.dhf = None
        self.id_mapping = None
    
    def create(self, args):
        super(KGramBenchmarker, self).create(args)

    def test(self, args):
        self.id_mapping = dict(izip(map(int, self.id_mapping[:,0]), self.id_mapping[:,1]))
        super(KGramBenchmarker, self).test(args)

    def is_in_database(self, name):
        return any(name in filepath for filepath in self.id_mapping.values())

    def create_database(self, hashes):
        if self.db is None:
            # create a database to store hashes
            self.db = np.zeros(shape=(2**HASH_WIDTH, KGRAM_BUCKET_SIZE), dtype=np.uint8)

        if self.hash_counts is None:
            self.hash_counts = np.zeros(2**DOC_ID_MAX_WIDTH, dtype=np.uint32)
        if self.dhf is None:
            self.dhf = np.zeros(2**HASH_WIDTH, dtype=np.uint32)

        if self.id_mapping is None:
            self.id_mapping = dict()
        else:
            self.id_mapping = dict(izip(map(int, self.id_mapping[:,0]), self.id_mapping[:,1]))

        docid_offset = KGRAM_BUCKET_SIZE
        if len(self.id_mapping) > 0:
            docid_offset = max(self.id_mapping.keys())

        total_hashes, dropped_hashes, dropped_hashes_self = 0, 0, 0

        for docid, (f, time_hashes) in enumerate(hashes):
            docid += docid_offset
            self.id_mapping[docid] = f

            arr_pos = docid % KGRAM_BUCKET_SIZE
            docid_compact = docid >> KGRAM_BUCKET_SIZE_WIDTH

            if docid % 100 == 0:
                print "indexing", docid
                print "arr_pos", arr_pos
                print "docid", docid
                print "docid_compact", docid_compact

            try:
                total_hashes += time_hashes.shape[0]
            except:
                continue

            hash_kgrams = create_kgrams(time_hashes, 1, KGRAM_WIDTH, HASH_WIDTH)

            for kgram in hash_kgrams:
                self.dhf[kgram] += 1
                curr_val = self.db[kgram, arr_pos]
                if curr_val == 0 or curr_val == docid_compact: # no collision
                    self.db[kgram, arr_pos] = docid_compact
                    self.hash_counts[docid] += 1
                else: # collision
                    dropped_hashes += 1

                    colliding_doc = (curr_val << KGRAM_BUCKET_SIZE_WIDTH) | arr_pos

                    colliding_hash_count = self.hash_counts[colliding_doc]
                    hash_count = self.hash_counts[docid]

                    rand = np.random.randint(0, hash_count + colliding_hash_count)
                    if rand > hash_count: # replace the existing pair with the new one
                        self.db[kgram, arr_pos] = docid_compact
                        self.hash_counts[colliding_doc] -= 1
                        self.hash_counts[docid] += 1

            if docid % 1 == 0:
                print self.db[kgram]
                print "dropped/total", dropped_hashes, total_hashes, \
                    (float(dropped_hashes)/float(total_hashes)), "self", \
                    float(dropped_hashes_self) / float(total_hashes)

        self.id_mapping = np.array(list(self.id_mapping.iteritems()))

        nonzero = np.count_nonzero(self.db)
        size = self.db.shape[0] * self.db.shape[1]

        print "STATS"

        print "nonzero", nonzero
        print "total", size
        print "%% filled", float(nonzero) / float(size)


    # docid = ((packed % 2**DOC_ID_COMPACT_WIDTH) << BUCKET_SIZE_WIDTH) | position
    # time = packed >> DOC_ID_COMPACT_WIDTH
    # return time, docid

    def match(self, time_hashes):
        row_indices = create_kgrams(time_hashes, 1, KGRAM_WIDTH, HASH_WIDTH)
        packed_rows = self.db[row_indices].astype(np.uint16)
        keep_vals  = np.where(packed_rows != 0)
        positions = np.tile(np.arange(2**KGRAM_BUCKET_SIZE_WIDTH), (len(row_indices), 1))

        row_doc_ids = (packed_rows << KGRAM_BUCKET_SIZE_WIDTH) | positions

        matches = np.zeros(2**DOC_ID_MAX_WIDTH, dtype=np.uint32)
        max_hashes = np.max(self.dhf)

        for i, hsh in enumerate(row_indices):
            matches[row_doc_ids[i]] += (max_hashes * max_hashes) / self.dhf[hsh]

        matches[:2**KGRAM_BUCKET_SIZE_WIDTH] = 0

        matches = dict(izip(row_doc_ids.flatten(), matches[row_doc_ids].flatten()))
        best_matches = sorted(matches.iteritems(), key=lambda r: r[1])[::-1]
        if len(best_matches) > 10:
            best_matches = best_matches[:10]

        matches_out = []
        for m in best_matches:
            matches_out.append((m[0], m[1], self.id_mapping[m[0]]))

        return matches_out, matches_out, matches_out

    def load_db(self, path):
        self.db = np.load(os.path.join(path, "db_kgram.npy"))
        self.hash_counts = np.load(os.path.join(path, "counts_kgram.npy"))
        self.dhf = np.load(os.path.join(path, "dhf_kgram.npy"))
        self.id_mapping = np.load(os.path.join(path, "id_mapping_kgram.npy"))

        print np.bincount(self.dhf)

    def save_db(self, path):
        np.save(os.path.join(path, "db_kgram.npy"), self.db)
        np.save(os.path.join(path, "counts_kgram.npy"), self.hash_counts)
        np.save(os.path.join(path, "dhf_kgram.npy"), self.dhf)
        np.save(os.path.join(path, "id_mapping_kgram.npy"), self.id_mapping)


def create_kgrams_with_width(hashes, k = 2, bits = 24):
    import sha

    def calc_hash(row):
        return np.uint32(int(sha.new(repr(row)).hexdigest()[:bits/4], 16))

    kgrams = []
    total = 0
    found = 0
    time_stamps = sorted(list(set(hashes[:, 0])))
    for ti in range(len(time_stamps) - k):
        hash_lists = []
        time_offsets = []
        for i in range(k):
            hash_lists.append(hashes[hashes[:, 0] == time_stamps[ti + i]][:, 1])
            # hash_lists.append(time_stamps[ti + i + 1] - time_stamps[ti + i])
            time_offsets.append(time_stamps[ti + i + 1] - time_stamps[ti + i])

        del time_offsets[-1]
        time_offsets.append(1)
        # print time
        grams = cartesian(*hash_lists)

        grams_expanded = []
        for toi, to in enumerate(time_offsets):
            for i in range(to):
                grams_expanded.append(grams[:, toi])

        grams = np.column_stack(grams_expanded)

        found += 1
        grams = np.apply_along_axis(calc_hash, 1, grams)
        kgrams.append(np.ones((1,), dtype=np.uint32) * np.min(grams))

        total += 1

        # print hash_lists
        # print grams
    # print "total vs has kgrams", total, found
    kgrams = np.concatenate(kgrams, axis=0)
    print kgrams
    return kgrams

if __name__ == "__main__":
    args = parse_args()

    benchmarker = args.parser_type()
    benchmarker.run()