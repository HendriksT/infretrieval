from audfprint import audfprint_analyze
import numpy as np
import os
import sqlite3
import zlib
from settings import create_analyzer, unpack
from collections import defaultdict
from time import time as tt

def mp3_file_names(dir):
    for a, _, d in os.walk(dir):
        for f in d:
            if f.endswith(".mp3"):
                yield os.path.join(a, f)

files = list(mp3_file_names("C:\Users\Wannes2\Desktop\Dropbox\_Muziek"))

conn = sqlite3.connect("data/songs400.db")

t = tt()

analyzer = create_analyzer(shifts=4)
time_hashes = analyzer.wavfile2hashes("galwaygirl4s.wav")

print len(time_hashes) / (4.0 * 27)

print "hashing", tt() - t
t = tt()

matches = defaultdict(int)
matches_doc = defaultdict(int)

time_hashes = [(33, 26393), (33, 238794), (33, 240153), (10027, 922531), (10027, 924387), (10061, 921794)]

curr = conn.cursor()

for time, hsh in time_hashes:
    curr.execute("SELECT hsh, packs FROM fingers WHERE hsh = ?", (hsh,))
    row = curr.fetchone()
    if row:
        hsh, packs = row[0], np.frombuffer(zlib.decompress(row[1]), np.uint16)

        unpacked = []

        for i, packed in enumerate(packs):
            if packed != 0:
                time_match, docid = unpack(packed, i)
                offset = time - time_match
                matches[(docid, offset)] += 1
                matches_doc[docid] += 1

                unpacked.append((offset, docid))

        print hsh, "&", " & ".join(map(str, unpacked)), "\\\\"

curr.close()

print "fetching", tt() - t

matches_doc = sorted(matches_doc.iteritems(), key=lambda r: r[1])

for docid, amount in matches_doc[-10:]:
    print amount, files[docid]

best_matches = defaultdict(int)
for (docid, offset), count in matches.iteritems():
    if docid not in best_matches:
        best_matches[docid] = count
    else:
        best_matches[docid] = max(best_matches[docid], count)

best_matches = sorted(best_matches.iteritems(), key=lambda r: r[1])

for docid, amount in best_matches[-10:]:
    print amount, files[docid]