#White noise mp3 taken from http://mc2method.org/white-noise/
#Conversations taken from
# TODO

import multiprocessing
import sys
import os
import random
import argparse
import pyaudio,wave
import threading
from pydub import * #http://pydub.com/
from random import randint
from time import sleep

PATH_TO_MUSIC       = "/home/uauser/Desktop/RetrievalMusic/"
PATH_TO_NOISE         = "/home/uauser/Desktop/Noises/"
PATH_TO_OUT         = "/home/uauser/Desktop/TestClips/"
NUMBER_OF_TESTS     = 1
TEST_CLIP_LENGTHS   = [4000,7000,10000,13000,17000,23600]
OUTPUT_DEVICE_INDEX = 1

class RecordingThread(threading.Thread):
    def __init__(self,RECORD_SECONDS,OUTPUT_FILENAME):
        threading.Thread.__init__(self)
        self.record_seconds = RECORD_SECONDS
        self.output_filename = OUTPUT_FILENAME

    def run(self):
        sleep(1)

        #From the Record example included in http://people.csail.mit.edu/hubert/pyaudio/
        CHUNK = 1024
        FORMAT = pyaudio.paInt32
        CHANNELS = 2
        RATE = 44100

        p = pyaudio.PyAudio()
        print p.get_default_input_device_info()

        stream = p.open(format=FORMAT,
                        channels=CHANNELS,
                        rate=RATE,
                        input=True,
                        frames_per_buffer=CHUNK)

        frames = []
        for i in range(0, int(RATE / CHUNK * self.record_seconds/1000)):
            data = stream.read(CHUNK)
            frames.append(data)

        stream.stop_stream()
        stream.close()
        p.terminate()

        wf = wave.open(self.output_filename, 'wb')
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(p.get_sample_size(FORMAT))
        wf.setframerate(RATE)
        wf.writeframes(b''.join(frames))
        wf.close()

class PlaybackThread(threading.Thread):
    def __init__(self,RECORD_SECONDS,INPUT_FILENAME,OUTPUT_DEVICE_INDEX):
        threading.Thread.__init__(self)
        self.record_seconds = RECORD_SECONDS
        self.input_filename = INPUT_FILENAME
        self.output_device_index = OUTPUT_DEVICE_INDEX

    def run(self):
        # Copied & adapted from https://stackoverflow.com/questions/26673746/playing-mp3-files-with-pyaudio
        # Open an audio segment & instantiate PyAudio
        CHUNK = 1024
        sound = randomSubClip(AudioSegment.from_file(self.input_filename), self.record_seconds)
        player = pyaudio.PyAudio()
        print player.get_default_output_device_info()

        stream = player.open(format = player.get_format_from_width(sound.sample_width),
            channels = sound.channels,
            # output_device_index = self.output_device_index,
            rate = sound.frame_rate,
            output = True)

        # PLAYBACK LOOP
        start = 0
        volume = 100.0
        playchunk = sound[0:self.record_seconds] - (60 - (60 * (volume/100.0)))
        millisecondchunk = 50 / 1000.0

        time = start
        for chunks in utils.make_chunks(playchunk, millisecondchunk*1000): #utils is pydub.utils
            time += millisecondchunk
            stream.write(chunks._data)
            if time >= start+self.record_seconds:
                break

        stream.close()
        player.terminate()

def randomSubClip(original_song, length_of_testclip):
    #original_song is an AudioSegment, these support indexing similar to lists where the indices are milliseconds in the segment
    #hence, take a slice of length_of_testclip from somewhere in the file
    start = randint(0,int(original_song.duration_seconds*1000)-10001) #-10001 to make sure we have enough left to get a clip out of
    return original_song[start:start+length_of_testclip]

def reduce_length_of_noises():
    # Ideally, this only needs to be run once for initial noises, reduces length of the noise files (initially 10 minutes) to 10 seconds each
    # Perhaps a version of 4s and one of 7s is helpful, leave that to be decided later
    for filename in os.listdir(PATH_TO_NOISE):
        AudioSegment.from_mp3(PATH_TO_NOISE+filename)[:10000].export(PATH_TO_NOISE+filename,format('mp3'))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--musicfolder', dest="PATH_TO_MUSIC", required=False,default = PATH_TO_MUSIC)
    parser.add_argument('-o', '--outputfolder', dest="PATH_TO_OUT", required=False,default = PATH_TO_OUT)
    parser.add_argument('-n', '--noisefolder', dest="PATH_TO_NOISE", required=False,default = PATH_TO_NOISE)
    parser.add_argument('-nr', '--numbertests', type=int,dest="NUMBER_OF_TESTS", required=False,default = NUMBER_OF_TESTS)
    parser.add_argument('-d', '--device', type=int,dest="OUTPUT_DEVICE_INDEX", required=False,default = OUTPUT_DEVICE_INDEX)
    args = parser.parse_args()

    #Reduce length of noise files from 10m to 10s
    #reduce_length_of_noises()

    args.PATH_TO_OUT   += "/" if args.PATH_TO_OUT[-1]   != '/' else ""
    args.PATH_TO_NOISE += "/" if args.PATH_TO_NOISE[-1] != '/' else ""
    args.PATH_TO_MUSIC += "/" if args.PATH_TO_MUSIC[-1] != '/' else ""

    PATH_TO_OUT   = args.PATH_TO_OUT  
    PATH_TO_NOISE = args.PATH_TO_NOISE
    PATH_TO_MUSIC = args.PATH_TO_MUSIC
    OUTPUT_DEVICE_INDEX = args.OUTPUT_DEVICE_INDEX

    for _ in range(args.NUMBER_OF_TESTS):
        test_clip_length = 4000
        test_clip_length = TEST_CLIP_LENGTHS[randint(0,len(TEST_CLIP_LENGTHS)-1)]

        # Select a random file from the listed music directory
        original_mp3_path = PATH_TO_MUSIC + random.choice(os.listdir(PATH_TO_MUSIC))

        while not original_mp3_path.endswith(".mp3") and not original_mp3_path.endswith(".m4a"):
            original_mp3_path = PATH_TO_MUSIC + random.choice(os.listdir(PATH_TO_MUSIC))

        #Choose now, either play it back and record it or overlay some noise
        #Lets usually choose the playback/record method
        if randint(0,10) >= 11: #Change 0 to a higher int if you want noise too
            # As suggested, use method of creating test clips by playing and recording music
            # Seems reasonable to include at least one recorded version of every song found, we could vary the length but still, these are most important

            ###########################################################
            ### REPLACED BY TRY_OUTPUT_DEVICES.PY, use that instead ###
            ###########################################################
            #Function that will show the indices for your system, best to find one that adresses the speaker (for me, this was 9)
            #This index must be passed to the play function. Some trial&error may be required
            # find_output_devices()
            # sys.exit(1)

            #Prepare the file to store the TestClip in:
            output_filename = args.PATH_TO_OUT+original_mp3_path.split('/')[-1]
            output_filename = ".".join(output_filename.split('.')[:-1])+'_test'+str(_)+"_"+str(test_clip_length)+'_rerecorded.wav'
            open(output_filename, "w+").close()

            #Constructor format is #seconds, name of the file to playback or record and , for playback, the device to use for playback
            threads = [
                RecordingThread(test_clip_length,output_filename),
                PlaybackThread(test_clip_length+2000,original_mp3_path,OUTPUT_DEVICE_INDEX),
            ]
            for t in threads:
                t.start()
            for t in threads:
                t.join()
        else:
            noise_number = str(randint(0,5))
            noise_level = randint(0,10)

            output_filename = os.path.join(args.PATH_TO_OUT, os.path.split(original_mp3_path)[-1])
            output_filename = ".".join(output_filename.split('.')[:-1])+'_test'+str(_)+'_with_noise'+noise_number+'_'+str(test_clip_length)+"_"+str(noise_level)+'_overlayed.mp3'

            #Get the song form... (lib supports any format ffmpeg supports, our files are stored as .mp3)
            original_song = AudioSegment.from_file(original_mp3_path)
            test_clip = randomSubClip(original_song, test_clip_length)

            # Play with the +Y integer, higher integer means more noise. 20 is pretty high, 0 is barely noticeable noise
            noise = AudioSegment.from_mp3(PATH_TO_NOISE+'noise'+noise_number+"_"+str(test_clip_length)+'.mp3')+randint(0,10)

            output = noise.overlay(test_clip)
            output.export(output_filename, format="mp3")
