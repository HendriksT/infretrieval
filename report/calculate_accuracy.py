from collections import defaultdict

experiments = defaultdict(dict)

with open("accuracy_data.txt", "r") as inf:
    parts = [p.replace("\\end{filecontents}", "") for p in inf.read().split("\\begin{filecontents}{") if "}" in p]

    for p in parts:
        title_end = p.index("}")
        title = p[:title_end]
        data  = [i.replace("\t", " ").split() for i in p[title_end+1:].strip().split("\n")]

        # print data

        accuracy = sum(float(r[1]) for r in data) / len(data)

        if "audf" in title:
            key = title.replace("audf", "").replace(".data", "")
            experiments[key]["audf"] = accuracy
        elif "ears" in title:
            key = title.replace("ears", "").replace(".data", "")
            experiments[key]["ears"] = accuracy
        else:
            print "cant parse name:", title
            exit(1)

    coords_audf = []
    coords_ears = []
    symbolic_coords = []

    experiments_after = dict()

    for k, accs in experiments.iteritems():
        k = k.strip("_").replace("_", "\\_")
        experiments_after[k] = accs
        symbolic_coords.append(k)
        coords_audf.append("({},{})".format(accs["audf"], k))
        coords_ears.append("({},{})".format(accs["ears"], k))

    command = """\\addplot coordinates {{{}}};"""

    symbolic_coords = sorted(symbolic_coords, key=lambda k: experiments_after[k]["ears"])

    print "%y coords"
    print ",".join(symbolic_coords)

    print "%ears"
    print command.format("".join(coords_ears))
    print "%audf"
    print command.format("".join(coords_audf))